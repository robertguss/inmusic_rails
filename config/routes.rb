Rails.application.routes.draw do

  resources :product_validators
  resources :codes do
    collection { post :import }
  end

  resources :softwares do
    resources :software_assigns
  end

  resources :products do
    resources :registrations
  end

  resources :software_downloads

  namespace :admin do
    DashboardManifest::DASHBOARDS.each do |dashboard_resource|
      resources dashboard_resource
    end
    root controller: DashboardManifest::ROOT_DASHBOARD, action: :index
  end

  root to: 'visitors#index'
  devise_for :users
  resources :users
end

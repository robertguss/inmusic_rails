class CreateSoftwareDownloads < ActiveRecord::Migration
  def change
    create_table :software_downloads do |t|
      t.references :software, index: true, foreign_key: true
      t.string :title
      t.string :url

      t.timestamps null: false
    end
  end
end

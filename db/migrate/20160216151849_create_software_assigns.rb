class CreateSoftwareAssigns < ActiveRecord::Migration
  def change
    create_table :software_assigns do |t|
      t.references :product, index: true, foreign_key: true
      t.references :software, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

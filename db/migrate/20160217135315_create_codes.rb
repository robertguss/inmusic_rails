class CreateCodes < ActiveRecord::Migration
  def change
    create_table :codes do |t|
      t.references :software, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.string :label
      t.string :code
      t.string :in_use

      t.timestamps null: false
    end
  end
end

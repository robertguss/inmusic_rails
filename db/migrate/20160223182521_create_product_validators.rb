class CreateProductValidators < ActiveRecord::Migration
  def change
    create_table :product_validators do |t|
      t.references :product, index: true, foreign_key: true
      t.string :validator

      t.timestamps null: false
    end
  end
end

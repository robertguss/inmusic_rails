class CreateSoftwares < ActiveRecord::Migration
  def change
    create_table :softwares do |t|
      t.string :title

      t.timestamps null: false
    end
  end
end

class AddSerialNumberToRegistration < ActiveRecord::Migration
  def change
    add_column :registrations, :serial_num, :string
  end
end

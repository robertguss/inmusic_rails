class ChangeNameOfSerialNumtoSerialNumber < ActiveRecord::Migration
  def change
    rename_column :registrations, :serial_num, :serial_number
  end
end

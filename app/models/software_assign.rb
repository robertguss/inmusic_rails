# == Schema Information
#
# Table name: software_assigns
#
#  id          :integer          not null, primary key
#  product_id  :integer
#  software_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class SoftwareAssign < ActiveRecord::Base
  belongs_to :product
  belongs_to :software

  accepts_nested_attributes_for :software, :product
end

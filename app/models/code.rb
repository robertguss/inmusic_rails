# == Schema Information
#
# Table name: codes
#
#  id          :integer          not null, primary key
#  software_id :integer
#  user_id     :integer
#  label       :string
#  code        :string
#  in_use      :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Code < ActiveRecord::Base
  belongs_to :software
  belongs_to :user
  accepts_nested_attributes_for :software, :user

  def self.import(file, software_id)
    CSV.foreach(file.path, headers: true) do |row|
      code = Code.new row.to_hash
      code.software_id = software_id
      code.save!
    end
  end

end

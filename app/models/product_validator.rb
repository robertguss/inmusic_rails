class ProductValidator < ActiveRecord::Base
  belongs_to :product

  validates :validator, presence: true
  validates :product, presence: true
end

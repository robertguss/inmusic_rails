# == Schema Information
#
# Table name: softwares
#
#  id         :integer          not null, primary key
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Software < ActiveRecord::Base
  has_many :software_assigns
  has_many :products, through: :software_assigns
  has_many :software_downloads
  has_many :codes
end

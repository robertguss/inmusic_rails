# == Schema Information
#
# Table name: products
#
#  id               :integer          not null, primary key
#  product_name     :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  product_image_id :string
#

class Product < ActiveRecord::Base
  attachment :product_image
  has_many :registrations
  has_many :users, through: :registrations
  has_many :software_assigns
  has_many :softwares, through: :software_assigns
  has_many :product_validators
end

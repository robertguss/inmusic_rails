# == Schema Information
#
# Table name: software_downloads
#
#  id          :integer          not null, primary key
#  software_id :integer
#  title       :string
#  url         :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class SoftwareDownload < ActiveRecord::Base
  belongs_to :software
  accepts_nested_attributes_for :software
end

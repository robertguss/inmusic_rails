# == Schema Information
#
# Table name: software_assigns
#
#  id          :integer          not null, primary key
#  product_id  :integer
#  software_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class SoftwareAssignsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_software_assign
  before_action :verify_admin

  def index
    @software_assign = SoftwareAssign.all
  end

  def new
    @software_assign = SoftwareAssign.new
  end

  def create
    @software_assign = SoftwareAssign.new(software_assigns_params)
    if @software_assign.save
      redirect_to software_software_assigns_path, :notice => 'Software was successfully registered to product.'
    else
      render :new, :alert => 'Unable to register software! Please try again.'
    end
  end

  private
    def software_assigns_params
      params.require(:software_assign).permit(:software_id, :product_id)
    end

    def set_software_assign
      @software = Software.find(params[:software_id])
    end

end



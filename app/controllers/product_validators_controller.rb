class ProductValidatorsController < ApplicationController
  before_action :set_product_validator, only: [:show, :edit, :update, :destroy]

  def index
    @product_validators = ProductValidator.all
  end

  def show
  end

  def new
    @product_validator = ProductValidator.new
  end

  def edit
  end

  def create
    @product_validator = ProductValidator.new(product_validator_params)

    if @product_validator.save
      redirect_to @product_validator, notice: 'Product validator was successfully created.'
    else
      render :new
    end
  end

  def update
    if @product_validator.update(product_validator_params)
      redirect_to @product_validator, notice: 'Product validator was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @product_validator.destroy
    redirect_to product_validators_url, notice: 'Product validator was successfully destroyed.'
  end

  private
    def set_product_validator
      @product_validator = ProductValidator.find(params[:id])
    end

    def product_validator_params
      params.require(:product_validator).permit(:product_id, :validator)
    end
end

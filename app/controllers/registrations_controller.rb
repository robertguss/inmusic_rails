# == Schema Information
#
# Table name: registrations
#
#  id            :integer          not null, primary key
#  user_id       :integer
#  product_id    :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  serial_number :string
#

class RegistrationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_product
  #before_action :verify_admin

  def index
    @registrations = @product.registrations
  end

  def new
    @registration = @product.registrations.new
    @validator = @product.product_validators.first.validator
  end

  def create
    @registration = @product.registrations.new(registration_params)
    @registration.user = current_user
    @validator = @product.product_validators.first.validator
    regex = Regexp.new(@validator)

    if @registration.serial_number =~ regex
      assign_code_to_user

      @registration.save
      redirect_to products_path, :notice => 'Product was successfully registered.'
    else
      redirect_to products_path, :alert => 'Incorrect Serial Number! Please Try Again'
    end
  end



  private
    def registration_params
      params.require(:registration).permit(:user_id, :product_id, :serial_number)
    end

    def set_product
      @product = Product.find(params[:product_id])
    end

    def assign_code_to_user
     @product.softwares.each do |software|
       software.codes.each do |code|
         if code.user_id == nil && code.software_id = software
         user_code = code
         user_code.update(user: current_user, software: software)
         break
         end
       end
     end
  end

end

# == Schema Information
#
# Table name: softwares
#
#  id         :integer          not null, primary key
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SoftwaresController < ApplicationController
  before_action :authenticate_user!
  before_action :verify_admin
  before_action :set_software, only: [:show, :edit, :update, :destroy]

  def index
    @softwares = Software.all
  end

  def show
  end

  def new
    @software = Software.new
  end

  def edit
  end

  def create
    @software = Software.new(software_params)
    if @software.save
      redirect_to softwares_path, :notice => 'Software title was successfully created.'
    else
      render :new, :alert => 'Unable to create software title! Please try again.'
    end
  end

  def update
    if @software.update(software_params)
      redirect_to softwares_path, :notice => 'Software title was successfully updated.'
    else
      render :edit, :alert => 'Unable to update software title! Please try again.'
    end
  end

  def destroy
    @software.destroy
    redirect_to softwares_path, :alert => 'Software title was successfully destroyed.'
  end


  private
    def set_software
      @software = Software.find(params[:id])
    end

    def software_params
      params.require(:software).permit(:title)
    end
end

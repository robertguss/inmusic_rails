class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def verify_admin
    if current_user.admin?
      return
    else
      redirect_to root_url, alert: 'Access Denied!'
    end
  end

  def unused_code?(code)
    code.user_id == nil
  end

  helper_method :unused_code?

end

# == Schema Information
#
# Table name: codes
#
#  id          :integer          not null, primary key
#  software_id :integer
#  user_id     :integer
#  label       :string
#  code        :string
#  in_use      :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class CodesController < ApplicationController
  before_action :authenticate_user!
  before_action :verify_admin
  before_action :set_code, only: [:show, :edit, :update, :destroy]

  def index
    @codes = Code.all
  end

  def csv
    send_file Rails.root.join('private', 'codes_template.csv'), :type=>"application/csv", :x_sendfile=>true
  end

  def show
  end

  def new
    @code = Code.new
  end

  def edit
  end

  def create
    Code.import(code_params[:code], code_params[:software_id])
    redirect_to codes_path, notice: 'Codes were successfully created.'
  end

  def update
    if @code.update(code_params)
      redirect_to @code, notice: 'Codes were successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @code.destroy
    respond_to do |format|
      redirect_to codes_url, notice: 'Codes were successfully destroyed.'
    end
  end

  def import
    Code.import(code_params[:code], code_params[:software_id])
    redirect_to codes_path, notice: 'Codes were successfully uploaded!'
  end

  private
    def clean_up_codes
     @codes.each do |code|
       if code.code[0] == '#'
         code.destroy
       end
     end
    end

    def set_code
      @code = Code.find(params[:id])
    end

    def code_params
      params.require(:code).permit(:software_id, :label, :code)
    end
end

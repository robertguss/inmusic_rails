# == Schema Information
#
# Table name: software_downloads
#
#  id          :integer          not null, primary key
#  software_id :integer
#  title       :string
#  url         :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class SoftwareDownloadsController < ApplicationController
  before_action :authenticate_user!
  before_action :verify_admin
  before_action :set_software, only: [:new, :edit, :update, :destroy]
  before_action :set_software_downloads, only: [:show, :edit, :update, :destroy]

  def index
    @software_downloads = SoftwareDownload.all
  end

  def show
  end

  def new
    @software_download = SoftwareDownload.new
  end

  def edit
  end

  def create
    @software_download = SoftwareDownload.new(software_downloads_params)
    if @software_download.save
      redirect_to software_downloads_path, :notice => 'Software link was successfully created.'
    else
      render :new, :alert => 'Unable to create software link! Please try again.'
    end
  end

  def update
    if @software_download.update(software_downloads_params)
      redirect_to software_downloads_path, :notice => 'Software link was successfully updated.'
    else
      render :edit, :alert => 'Unable to update software link! Please try again.'
    end
  end

  def destroy
    @software_download.destroy
    redirect_to software_downloads_path, :alert => 'Software link was successfully destroyed.'
  end


  private
    def set_software_downloads
      @software_download = SoftwareDownload.find(params[:id])
    end

    def set_software
      @software = Software.all
    end

    def software_downloads_params
      params.require(:software_download).permit(:software_id, :title, :url)
    end
end


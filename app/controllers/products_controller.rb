# == Schema Information
#
# Table name: products
#
#  id               :integer          not null, primary key
#  product_name     :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  product_image_id :string
#

class ProductsController < ApplicationController
  before_action :authenticate_user!
  before_action :verify_admin, except: [:index, :show]
  before_action :set_product, only: [:show, :edit, :update, :destroy]

  def index
    @products = Product.all
  end

  def show
  end

  def new
    @product = Product.new
  end

  def edit
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      redirect_to @product, :notice => 'Product was successfully created.'
    else
      render :new, :alert => 'Unable to create product! Please try again.'
    end
  end

  def update
    if @product.update(product_params)
      redirect_to @product, :notice => 'Product was successfully updated.'
    else
      render :edit, :alert => 'Unable to update product! Please try again.'
    end
  end

  def destroy
    @product.destroy
    redirect_to products_url, :alert => 'Product was successfully destroyed.'
  end

  private
    def set_product
      @product = Product.find(params[:id])
    end

    def product_params
      params.require(:product).permit(:product_name, :product_image, :remove_product_image)
    end
end

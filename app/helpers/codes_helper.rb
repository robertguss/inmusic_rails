# == Schema Information
#
# Table name: codes
#
#  id          :integer          not null, primary key
#  software_id :integer
#  user_id     :integer
#  label       :string
#  code        :string
#  in_use      :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

module CodesHelper
end

# == Schema Information
#
# Table name: softwares
#
#  id         :integer          not null, primary key
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :software do
    title "MyString"
  end
end

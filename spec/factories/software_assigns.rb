# == Schema Information
#
# Table name: software_assigns
#
#  id          :integer          not null, primary key
#  product_id  :integer
#  software_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :software_assign do
    product nil
    software nil
  end
end

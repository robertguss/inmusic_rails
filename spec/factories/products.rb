# == Schema Information
#
# Table name: products
#
#  id               :integer          not null, primary key
#  product_name     :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  product_image_id :string
#

FactoryGirl.define do
  factory :product do
    product_name "MyString"
  end
end

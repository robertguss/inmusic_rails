# == Schema Information
#
# Table name: codes
#
#  id          :integer          not null, primary key
#  software_id :integer
#  user_id     :integer
#  label       :string
#  code        :string
#  in_use      :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :code do
    software nil
    user nil
    label "MyString"
    code "MyString"
    in_use "MyString"
  end
end

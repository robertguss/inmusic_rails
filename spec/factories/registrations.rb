# == Schema Information
#
# Table name: registrations
#
#  id            :integer          not null, primary key
#  user_id       :integer
#  product_id    :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  serial_number :string
#

FactoryGirl.define do
  factory :registration do
    user nil
    product nil
  end
end

# == Schema Information
#
# Table name: software_downloads
#
#  id          :integer          not null, primary key
#  software_id :integer
#  title       :string
#  url         :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :software_download do
    software nil
    title "MyString"
    url "MyString"
  end
end
